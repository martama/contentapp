from webapp import webApp

class contentApp(webApp):
    dicc_recursos = {'': 'PAG_PRINCIPAL', 'info':'PAG_INFO', 'favicon.ico':'PAG_PRINCIPAL'}

    def Analyze(self, request):
        #me devolverá el recurso que me estén pidiendo
        barra_recurso = request.split(" ")[1]
        recurso = barra_recurso.split("/")[1]
        return recurso


    def Compute(self, recurso):
        print(f"EL RECURSO ES: {recurso}")
        #para ese recurso lo comprobamos si está en el diccionario y servimos lo correspondiente
        html_body = "<html><body>"
        if recurso in self.dicc_recursos:
            page = self.dicc_recursos[recurso]
            if page == 'PAG_PRINCIPAL':
                print("ENTROOO")
                code = "200 OK"
                html_body += "<h1>Welcome to the main page</h1>" \
                    + "<p>This is a test page for a SARO exercise.</p>"
            if page == 'PAG_INFO':
                code = "200 OK"
                html_body += "<h1>Information</h1>" \
                    + "<p>This page is part of exercise 19.1 Clase contentApp</p>"
        else:
            code = "404 Not Found"
            html_body += "<h1>Not Found</h1>" \
                         + "<p>The requested URL was not found on this server.</p>"
        html_body += "</body></html>\r\n"
        return code, html_body


if __name__ == "__main__":
    webapp = contentApp()