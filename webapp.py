import socket
import argparse
import socketserver

PORT = 1234


def parse_args():
    parser = argparse.ArgumentParser(description="HTTP server")
    parser.add_argument('-p', '--port', type=int, default=PORT,
                        help="TCP port for the server")
    args = parser.parse_args()
    return args

class webApp:

    def Analyze(self, request):
        # Analiza la peticion recibida del navegador
        return None

    def Compute(self, recurso):
        # Devuelve una lista con el código resultante y el cuerpo de la página HTML
        code = " 200 OK "
        html_body = "<html><body><h1>Prueba de página"\
                   + "</h1></body></html>" \
                   + "\r\n"

        return (code, html_body)


    def __init__(self):
        args = parse_args()
        mySocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        mySocket.bind(("", args.port))
        print(f"Serving at port {args.port}")
        mySocket.listen(5)

        while True:
            print("Waiting for connections...")
            (recvSocket, address) = mySocket.accept()
            request = recvSocket.recv(2048).decode('utf-8')
            print(request)
            #web = webApp()
            recurso = self.Analyze(request)
            (code, html_body) = self.Compute(recurso)
            response_page = "HTTP/1.1 " + code + " \r\n\r\n" + html_body
            recvSocket.send(response_page.encode('utf8'))
            recvSocket.close()


if __name__ == "__main__":
    web = webApp()
